package newcursor.mn.ebarimt.offline;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by tsolmon on 3/9/17.
 */

public class AutoStart extends BroadcastReceiver
{
    public void onReceive(Context context, Intent arg1)
    {
        Intent intent = new Intent(context, AutoSendService.class);
        context.startService(intent);
        Log.i("Autostart", "started");
    }
}