package newcursor.mn.ebarimt.offline;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class AutoSendService extends Service {
	public static final String TAG = "VENTRACE";
	private static Timer _timer = new Timer();
	private DataHelper _dh = null;
	private static final int HELLO_ID = 99111;
	private String _accessToken = "";

	@Override
	public void onCreate() {
		super.onCreate();
		startService();
	}

	public AutoSendService() {
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	private void startService()
    {
		_dh = new DataHelper(this);
		_timer.scheduleAtFixedRate(new mainTask(), 0, 2*60*1000);
    }
	
	private class mainTask extends TimerTask
    { 
        public void run() 
        {
            toastHandler.sendEmptyMessage(0);
        }
    }
	
	private final Handler toastHandler = new Handler()
    {
		@Override
		public void handleMessage(Message msg)
        {
			Thread thr = new Thread(null, getNewsList, "");
			thr.start();
        }
    };


	@Override
	public void onDestroy() {
		if(_dh != null){
			_dh.close();
		}
		_timer.cancel();
		super.onDestroy();
	}
	
	private Runnable getNewsList = new Runnable() {
		@Override
		public void run() {
			checkSettings();
		}
	};
	private void checkSettings() {
		_dh.clearLottery();
		if(_dh.selectSettings("auto_send") && checkWifi()){
			login();
		}

		if(_dh.selectSettings("show_notification")) {
			createNotification();
		} else {
			deleteNotification();
		}
	}

	private boolean checkWifi(){
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		return mWifi.isConnected();
	}

	private void createNotification(){
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		CharSequence tickerText = getString(R.string.app_name);
		CharSequence contentTitle = getString(R.string.scan);
		Intent notificationIntent = new Intent(this, MainActivity.class);
		notificationIntent.putExtra("mn.newcursor.ebarimt.notifyid", HELLO_ID);
		PendingIntent contentIntent = PendingIntent.getActivity(this, HELLO_ID, notificationIntent, 0);
		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Notification mNotification = new Notification.Builder(this)
				.setContentTitle(tickerText)
				.setContentText(contentTitle)
				.setSmallIcon(R.mipmap.ebarimt_grey)
				.setContentIntent(contentIntent)
				.setPriority(Notification.PRIORITY_MAX)
				.setAutoCancel(false)
				.setOngoing(true)
				.build();
		//.setSound(alarmSound)

		mNotificationManager.notify(HELLO_ID, mNotification);
	}

	private void deleteNotification(){
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		mNotificationManager.cancel(HELLO_ID);
	}

	public void login(){
		if(_accessToken.length() > 0){
			sendData();
		} else {
			final HashMap<String, String> user = _dh.selectUser();
			if(user.size() > 0) {
				String url = "https://mobile.ebarimt.mn/rest/login/check";
				StringRequest strReq = new StringRequest(Request.Method.POST,
						url, new Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						//Log.e(TAG, "response: " + response);
						try {
							JSONObject jsonObject = new JSONObject(response);
							_accessToken = jsonObject.getString("access_token");
							//Log.e(TAG, "access_token: " + _accessToken);
							if (_accessToken.length() > 0) {
								sendData();
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						//Log.e(TAG, "Login Error: " + error.getMessage());
					}
				}) {

					@Override
					protected Map<String, String> getParams() {
						Map<String, String> params = new HashMap<>();
						params.put("loginname", user.get("username"));
						params.put("password", user.get("password"));
						params.put("appId", "ZvUU9LUEVzaWRmR0JORk");
						params.put("appSecret", "N2FtWnZVVTlMVUVWemFX");

						return params;
					}
				};

				MySingleton.getInstance(getApplicationContext()).addToRequestQueue(strReq);
			}
		}
	}

	public void sendData(){
		//Log.e(TAG, "sendData");
		String url = "https://mobile.ebarimt.mn/rest/pos/approveQr";
		List<DataItem> datas = _dh.selectLottery(false);
		for (final DataItem di : datas) {
			StringRequest strReq2 = new StringRequest(Request.Method.POST,
					url, new Response.Listener<String>() {

				@Override
				public void onResponse(String response) {
					//Log.e(TAG, "Service: " + response);
					try {
						JSONObject jsonObject = new JSONObject(response);
						int status = jsonObject.getInt("status");
						if(status == 1 || status == 2){
							_dh.sentLottery(di.getId(), status + "");
							//_dh.deleteLottery(di.getId());
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}, new Response.ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError error) {
					if(error.networkResponse.statusCode == 500){
						_dh.sentLottery(di.getId(), "-1");
						//_dh.deleteLottery(di.getId());
					}
					//Log.e(TAG, "Service Error: " + error.getMessage());
				}
			}) {
				@Override
				public String getBodyContentType() {
					return "application/json; charset=utf-8";
				}

				@Override
				public byte[] getBody() throws AuthFailureError {
					JSONObject params = new JSONObject();
					try {
						params.put("accessToken", _accessToken);
						params.put("data", di.getData());
					} catch (JSONException e) {
						e.printStackTrace();
					}

					return params.toString().getBytes();
				}
			};

			MySingleton.getInstance(getApplicationContext()).addToRequestQueue(strReq2);
		}
	}
}
