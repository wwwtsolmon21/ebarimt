package newcursor.mn.ebarimt.offline;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tsolmon on 2017-03-09.
 */
public class NewsListAdapter extends BaseAdapter {
    private List<DataItem> _packages;
    private LayoutInflater mInflater;
    private Context _c;

    public NewsListAdapter(List<DataItem> packages, Context context) {
        _packages = packages;
        _c = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return _packages.size();
    }

    @Override
    public Object getItem(int position) {
        return _packages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_data, null);
            holder = new ViewHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvAdd = (TextView) convertView.findViewById(R.id.tv_add);
            holder.tvSend = (TextView) convertView.findViewById(R.id.tv_send);
            holder.llSend = (LinearLayout) convertView.findViewById(R.id.ll_send);
            holder.tvResponse = (TextView) convertView.findViewById(R.id.tv_response);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(_packages.get(position).getData().length() > 30){
            holder.tvTitle.setText(_packages.get(position).getData().substring(0, 30) + "...");
        } else {
            holder.tvTitle.setText(_packages.get(position).getData());
        }
        holder.tvAdd.setText(_packages.get(position).getDatetime());
        if(_packages.get(position).getSendDatetime().length() > 0){
            holder.tvSend.setText(_packages.get(position).getSendDatetime());
            holder.llSend.setVisibility(View.VISIBLE);
        } else {
            holder.llSend.setVisibility(View.GONE);
        }
        if(_packages.get(position).getResponse().equals("-1")){
            holder.tvResponse.setVisibility(View.VISIBLE);
        } else {
            holder.tvResponse.setVisibility(View.GONE);
        }

        return convertView;
    }

    class ViewHolder {
        TextView tvTitle, tvAdd, tvSend, tvResponse;
        LinearLayout llSend;
    }

    public void setProducts(List<DataItem> items) {
        _packages.clear();
        _packages.addAll(items);
        notifyDataSetChanged();
    }

}
