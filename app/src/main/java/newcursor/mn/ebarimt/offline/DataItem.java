package newcursor.mn.ebarimt.offline;

import static android.R.attr.type;

/**
 * Created by user on 2016-05-17.
 */
public class DataItem {
    public final static int HELP_TYPE_OTHER = 0;
    public final static int HELP_TYPE_ORDER = 1;
    public final static int HELP_TYPE_SATELLITE = 2;
    public final static int HELP_TYPE_FAQ = 3;
    public final static int HELP_TYPE_BRANCH = 4;
    public final static int HELP_TYPE_CALL_CENTER = 5;

    String _id;
    String _data;
    String _datetime = "";
    String _sendDatetime = "";
    String _response = "";

    public DataItem(String id, String data, String datetime) {
        _id = id;
        _data = data;
        _datetime = datetime;
    }

    public DataItem(String id, String data, String datetime, String sendDatetime) {
        _id = id;
        _data = data;
        _datetime = datetime;
        _sendDatetime = sendDatetime;
    }

    public DataItem(String id, String data, String datetime, String sendDatetime, String response) {
        _id = id;
        _data = data;
        _datetime = datetime;
        _sendDatetime = sendDatetime;
        _response = response;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        _id = id;
    }

    public String getData() {
        if(_data == null){
            return "";
        }
        return _data;
    }

    public void setData(String data) {
        _data = data;
    }

    public String getDatetime() {
        return _datetime;
    }

    public void setDatetime(String datetime) {
        _datetime = datetime;
    }

    public String getSendDatetime() {
        if(_sendDatetime == null){
            return "";
        }
        return _sendDatetime;
    }

    public void setSendDatetime(String sendDatetime) {
        _sendDatetime = sendDatetime;
    }

    public String getResponse() {
        if(_response == null){
            return "";
        }
        return _response;
    }

    public void setResponse(String response) {
        _response = response;
    }
}
