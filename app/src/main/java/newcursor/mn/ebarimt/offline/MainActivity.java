package newcursor.mn.ebarimt.offline;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public static final int BAR_CODE = 1;
    public static final String TAG = "VENTRACE";
    private static String _URL = "app/login";
    private boolean _retreiving = false;
    private DataHelper _dh = null;
    private String _accessToken = "", _userName = "", _userPass = "";
    ProgressDialog _progressDialog = null;
    private static final int HELLO_ID = 99111;
    int notifyid = -1;

    ListView _lvData;
    NewsListAdapter _chListAdapter;
    private List<DataItem> _datas = new ArrayList<DataItem>();

    private Button _bSend;
    private boolean scanned = false;
    private CheckBox _chbAutoSend, _chbShowNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _dh = new DataHelper(this);

        Intent intent = new Intent(this, AutoSendService.class);
        startService(intent);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button bScan = (Button) findViewById(R.id.b_scan);
        bScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scan();
            }
        });
        _bSend = (Button) findViewById(R.id.b_send);
        _bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(_accessToken.length() > 0) {
                    sendData();
                } else {
                    showLogin();
                }
            }
        });

        _lvData = (ListView) findViewById(R.id.lv_data);
        _chListAdapter = new NewsListAdapter(_datas, getApplicationContext());
        _lvData.setAdapter(_chListAdapter);
        _lvData.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        _lvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
    }

    @Override
    protected void onResume() {
        if(_dh != null){
            _datas = _dh.selectLottery(true);
            //Log.e(TAG, "COUNT: " + _datas.size());
            _chListAdapter.setProducts(_datas);
        }


        Bundle b = getIntent().getExtras();
        if(b != null && scanned == false){
            notifyid = b.getInt("mn.newcursor.ebarimt.notifyid");
            if(notifyid != -1 && (notifyid == HELLO_ID)) {
                scan();
            }
        }
        super.onResume();
    }

    private void scan(){
        scanned = false;
        IntentIntegrator integrator = new IntentIntegrator(
                MainActivity.this);
        integrator.initiateScan();
    }

    private void showLogin(){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompts_login, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.et_username);
        final EditText userPassword = (EditText) promptsView
                .findViewById(R.id.et_password);
        HashMap<String, String> user = _dh.selectUser();
        userInput.setText(user.get("username"));
        userPassword.setText(user.get("password"));
        builder.setTitle(getString(R.string.login))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.login), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        _userName = userInput.getText().toString();
                        _userPass = userPassword.getText().toString();
                        login();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showSettings(){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompts_settings, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setView(promptsView);

        _chbAutoSend = (CheckBox) promptsView
                .findViewById(R.id.chb_auto_send);
        _chbShowNotification = (CheckBox) promptsView
                .findViewById(R.id.chb_show_notification);
        _chbAutoSend.setChecked(_dh.selectSettings("auto_send"));
        _chbShowNotification.setChecked(_dh.selectSettings("show_notification"));
        builder.setTitle(getString(R.string.action_settings))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        _dh.updateSettings("auto_send", _chbAutoSend.isChecked());
                        _dh.updateSettings("show_notification", _chbShowNotification.isChecked());
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showAbout(){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompts_about, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(promptsView);
        builder.setTitle(getString(R.string.action_about))
                .setCancelable(true)
                .setPositiveButton(getString(R.string.ok), null);
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showDialog(){
        _progressDialog = new ProgressDialog(MainActivity.this);
        _progressDialog.setIndeterminate(true);
        _progressDialog.setMessage(getString(R.string.loading));
        _progressDialog.show();
    }

    private void hideDialog(){
        _progressDialog.dismiss();
    }

    public void login(){
        if(_retreiving)return;
        _retreiving = true;

        String url = "https://mobile.ebarimt.mn/rest/login/check";
        if(_userName.contains("@")){
            url += "Email";
        }
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();
                _retreiving = false;
                //Log.e(TAG, "response: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    _accessToken = jsonObject.getString("access_token");
                    //Log.e(TAG, "access_token: " + _accessToken);
                    if(_accessToken.length() > 0){
                        _dh.deleteUser();
                        _dh.insertUser(_userName, _userPass, "", _accessToken);
                        sendData();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                _retreiving = false;
                //Log.e(TAG, "Error: " + error.getMessage());
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(_userName.contains("@") ? "email" : "loginname", _userName);
                params.put("password", _userPass);
                params.put("appId", "ZvUU9LUEVzaWRmR0JORk");
                params.put("appSecret", "N2FtWnZVVTlMVUVWemFX");

                return params;
            }
        };

        showDialog();
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    public void sendData(){
        String url = "https://mobile.ebarimt.mn/rest/pos/approveQr";
        List<DataItem> datas = _dh.selectLottery(false);
        for (final DataItem di : datas) {
            StringRequest strReq2 = new StringRequest(Request.Method.POST,
                    url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    hideDialog();
                    //Log.d(TAG, response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        int status = jsonObject.getInt("status");
                        if(status == 1 || status == 2){
                            _dh.sentLottery(di.getId(), status + "");
                            //_dh.deleteLottery(di.getId());
                            //_datas.remove(di);
                            refreshList();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    hideDialog();
                    if(error.networkResponse.statusCode == 500){
                        _dh.sentLottery(di.getId(), "-1");
                        refreshList();
                    }
                    //Log.d(TAG, "Error: " + error.getMessage());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    JSONObject params = new JSONObject();
                    try {
                        params.put("accessToken", _accessToken);
                        params.put("data", di.getData());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    return params.toString().getBytes();
                }
            };

            showDialog();
            MySingleton.getInstance(getApplicationContext()).addToRequestQueue(strReq2);
        }
    }

    private void refreshList(){
        _datas = _dh.selectLottery(true);
        _chListAdapter.setProducts(_datas);
    }

    @Override
    public void onBackPressed() {
        openExitDialog();
    }

    private void openExitDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.are_you_exit))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(requestCode == BAR_CODE || true){
            scanned = true;
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode,
                    resultCode, intent);
            if (result != null) {
                String contents = result.getContents();
                if (contents != null) {
                    //Log.e(TAG, contents);
                    _dh.insertLottery(contents);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            showSettings();
            return true;
        } else if (id == R.id.action_about) {
            showAbout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
