package newcursor.mn.ebarimt.offline;

/**
 * Created by user on 2016-12-27.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.text.format.DateFormat;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.R.attr.id;
import static android.R.attr.password;
import static android.R.attr.type;
import static android.R.attr.value;
import static android.R.id.list;

public class DataHelper {

    private static final String DATABASE_NAME = "ebarimt.sqlite";
    private static final int DATABASE_VERSION = 3;

    private Context context;
    private SQLiteDatabase db;

    private SQLiteStatement insertStmt;

    public DataHelper(Context context) {
        this.context = context;
        OpenHelper openHelper = new OpenHelper(this.context);
        this.db = openHelper.getWritableDatabase();
    }

    public void updateUsername(String registration_id) {
        ContentValues update = new ContentValues();
        update.put("username", registration_id);
        //db.update("workers", update, "id=?", new String[]{id});
        db.update("users", update, null, null);
    }

    public void insertUser(String username, String password, String datetime, String accessToken) {
        ContentValues update = new ContentValues();
        update.put("username", username);
        update.put("password", password);
        update.put("datetime", datetime);
        update.put("access_token", accessToken);
        try {
            Long l = db.insertOrThrow("users", null, update);
        } catch (Exception e) {
            //catch code
        }
    }

    public void deleteUser() {
        db.delete("users", null, null);
    }

    public void deleteUser2(String id) {
        db.delete("users", "id=?", new String[]{id});
    }

    public void saveUsername(String username) {
        ContentValues update = new ContentValues();
        update.put("username", username);

        Cursor cursor = this.db.query(true, "users", new String[] {
                        "id", "worker_id"},
                null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                //db.update("workers", update, "id=?", new String[]{id});
                db.update("users", update, null, null);
                break;
            } while (cursor.moveToNext());
        } else {
            try {
                Long l = db.insertOrThrow("users", null, update);
            } catch (Exception e) {
                //catch code
            }
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    public String checkLogin() {
        String result = "";
        Cursor cursor = this.db.query(true, "users", new String[] {"id", "username"},
                null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                result = cursor.getString(1);
                break;
            } while (cursor.moveToNext());
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return result;
    }

    public void logout() {
        db.delete("users", null, null);
    }

    public HashMap<String, String> selectUser() {
        HashMap<String, String> str = new HashMap<String,String>();
        Cursor cursor = this.db.query(true, "users", new String[] {
                        "id", "username", "password", "datetime", "access_token"},
                null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                str.put("id", cursor.getString(0));
                str.put("username", cursor.getString(1));
                str.put("password", cursor.getString(2));
                str.put("datetime", cursor.getString(3));
                str.put("access_token", cursor.getString(4));
                break;
            } while (cursor.moveToNext());
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return str;
    }



    public void insertLottery(String data) {
        Date d = new Date();
        CharSequence sysdate  = DateFormat.format("yyyy-MM-dd HH:mm:ss", d.getTime());
        ContentValues update = new ContentValues();
        update.put("data", data);
        update.put("datetime", sysdate + "");
        try {
            Long l = db.insertOrThrow("lotteries", null, update);
        } catch (Exception e) {
            //catch code
        }
    }

    public List<DataItem> selectLottery(boolean isAll) {
        List<DataItem> list = new ArrayList<DataItem>();
        Cursor cursor;
        if(isAll){
            cursor = this.db.query(true, "lotteries", new String[] {
                            "id", "data", "datetime", "send_datetime", "response"},
                    null, null, null, null, "id DESC", null);
        } else {
            cursor = this.db.query(true, "lotteries", new String[]{
                            "id", "data", "datetime", "send_datetime", "response"},
                    "response IS NULL", null, null, null, null, null);
        }
        if (cursor.moveToFirst()) {
            do {
                DataItem di = new DataItem(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
                list.add(di);
            } while (cursor.moveToNext());
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    public void sentLottery(String id, String response) {
        Date d = new Date();
        CharSequence sysdate  = DateFormat.format("yyyy-MM-dd HH:mm:ss", d.getTime());
        ContentValues update = new ContentValues();
        update.put("response", response);
        update.put("send_datetime", sysdate + "");
        db.update("lotteries", update, "id=?", new String[]{ id });
    }

    public void deleteLottery(String id) {
        db.delete("lotteries", "id=?", new String[]{id});
    }

    public void clearLottery() {
        CharSequence sysdate  = DateFormat.format("yyyy-MM-dd HH:mm:ss", yesterday());
        db.delete("lotteries", "send_datetime < ?", new String[]{sysdate + ""});
    }

    private Date yesterday() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public HashMap<String, String> selectLottery2() {
        HashMap<String, String> str = new HashMap<String,String>();
        Cursor cursor = this.db.query(true, "lotteries", new String[] {
                        "id", "data", "datetime"},
                null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                str.put("id", cursor.getString(0));
                str.put("username", cursor.getString(1));
                str.put("datetime", cursor.getString(2));
                break;
            } while (cursor.moveToNext());
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return str;
    }

    public boolean selectSettings(String type) {
        boolean result = false;
        Cursor cursor = this.db.query(true, "settings", new String[] {"value"},
                "type='" + type + "'", null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                return cursor.getString(0).equals("1");
            } while (cursor.moveToNext());
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return result;
    }

    public void updateSettings(String type, boolean value) {
        ContentValues update = new ContentValues();
        String strValue = value ? "1" : "0";
        update.put("value", strValue);
        db.update("settings", update, "type=?", new String[]{ type });
    }

    public void close() {
        this.db.close();
    }

    private static class OpenHelper extends SQLiteOpenHelper {

        OpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            //db.execSQL("CREATE  TABLE \"workers\" (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"worker_id\" VARCHAR NOT NULL  UNIQUE , \"worker_key\" VARCHAR, \"registration_id\" VARCHAR, \"datetime\" DATETIME)");
            db.execSQL("CREATE TABLE \"users\" (\"id\" INTEGER PRIMARY KEY  NOT NULL ,\"username\" VARCHAR NOT NULL  DEFAULT (null) ,\"password\" VARCHAR DEFAULT (null) ,\"datetime\" DATETIME, \"access_token\" VARCHAR)");
            db.execSQL("CREATE TABLE \"lotteries\" (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"data\" VARCHAR, \"datetime\" DATETIME DEFAULT CURRENT_TIMESTAMP, \"send_datetime\" DATETIME, \"response\" VARCHAR)");
            db.execSQL("CREATE TABLE \"settings\" (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"type\" VARCHAR NOT NULL  UNIQUE , \"value\" VARCHAR NOT NULL  DEFAULT 1)");

            db.execSQL("INSERT INTO \"settings\" (\"type\",\"value\") VALUES ('auto_send', '1')");
            db.execSQL("INSERT INTO \"settings\" (\"type\",\"value\") VALUES ('show_notification', '1')");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("Example", "Upgrading database, this will drop tables and recreate.");
            db.execSQL("DROP TABLE IF EXISTS users");
            db.execSQL("DROP TABLE IF EXISTS lotteries");
            db.execSQL("DROP TABLE IF EXISTS settings");
            onCreate(db);
        }

    }
}
